namespace Infrastructure.StateMachine
{
    public interface IExitState
    {
        void Exit();
    }
}