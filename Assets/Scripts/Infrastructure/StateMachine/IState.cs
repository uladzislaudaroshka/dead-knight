namespace Infrastructure.StateMachine
{
    public interface IState : IExitState
    {
        void Enter();
    }
}