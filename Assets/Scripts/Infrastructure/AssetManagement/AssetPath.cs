namespace Infrastructure.AssetManagement
{
    public static class AssetPath
    {
        public const string HeroPath = "Hero/RPGHeroHP";
        public const string HudPath = "HUD/HUD";
    }
}